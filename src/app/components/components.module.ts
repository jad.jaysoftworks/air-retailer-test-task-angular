import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HeaderComponent} from "./header/header.component";
import {ChartComponent} from "./chart/chart.component";
import {MatButtonModule} from "@angular/material/button";
import {MatTabsModule} from "@angular/material/tabs";
import {MatChipsModule} from "@angular/material/chips";
import {MatCardModule} from "@angular/material/card";

@NgModule({
  declarations: [
    HeaderComponent,
    ChartComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatTabsModule,
    MatChipsModule,
    MatCardModule,
  ],
  exports: [
    HeaderComponent,
    ChartComponent,
  ]
})
export class ComponentsModule { }
