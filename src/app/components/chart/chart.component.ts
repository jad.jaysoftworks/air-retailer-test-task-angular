import { Component, OnInit } from '@angular/core';
import axios from "axios";
import {BinanceApiElement, PoloniexApiElement} from "../../interfaces/api";
import {FormControl} from "@angular/forms";
import * as echart from 'echarts';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  page: 'binance' | 'custom' = 'binance';
  binanceData: BinanceApiElement[] = [];
  customData: PoloniexApiElement[] = [];
  selectedElement = new FormControl(null);
  displayValue: BinanceApiElement | undefined;

  // Chart Data
  option = {
    xAxis: {
      data: ['2017-10-24', '2017-10-25', '2017-10-26', '2017-10-27']
    },
    yAxis: {},
    series: [
      {
        type: 'candlestick',
        data: [
          [20, 34, 10, 38],
          [40, 35, 30, 50],
          [31, 38, 33, 44],
          [38, 15, 5, 42]
        ]
      }
    ]
  };

  constructor() { }

  changeTab(index: number) {
    this.page = index === 0 ? 'binance' : 'custom';
    if (this.page === 'custom') {
      setTimeout(() => {
        this.generateCustomChartData();
      }, 500)
    }
  }

  ngOnInit(): void {
    this.fetchBinanceData();
    this.fetchCustomApiData();
    this.selectedElement.valueChanges.subscribe((value) => {
      this.displayValue = this.binanceData.find((element) => element.symbol === value);
    });
  }

  async fetchBinanceData() {
    const response = await axios.get('https://api.binance.com/api/v1/ticker/24hr');
    this.binanceData = response.data;
  }

  async fetchCustomApiData() {
    const response = await axios.get('https://poloniex.com/public?command=returnChartData&currencyPair=BTC_ETH&start=1496394399&end=9999999999&period=14400');
    this.customData = response.data;
  }

  private generateCustomChartData() {
    const labels: string[] = [];
    const data: number[][] = [];
    this.customData.forEach((element) => {
      labels.push(new Date(element.date).toDateString());
      data.push([element.open, element.high, element.low, element.close]);
    })
    this.option = {
      xAxis: {
        data: labels
      },
      yAxis: {},
      series: [
        {
          type: 'candlestick',
          data: data
        }
      ]
    };
    const chart = echart.init(document.getElementById('chart-canvas') as HTMLDivElement);
    chart.setOption(this.option);
  }
}
